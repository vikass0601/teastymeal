<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class DB {

    private static $_instance = null;
    private $_pdo, 
            $_query, 
            $_error = false, 
            $_results, 
            $_count = 0;
    
    private function __construct() {
        try{
            $this->_pdo = new PDO( 'mysql:host=' . Config::get( 'mysql/host' ) . ';dbname='. Config::get( 'mysql/db' ), Config::get( 'mysql/username' ), Config::get( 'mysql/password' ) );
        } catch ( PDOException $e){
            die( $e->getMessage() );
        }
    }
    
    public static function getInstnace(){
        if ( !isset (  self::$_instance ) ) {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }
    
    public function query ( $sql, $params = array() ) {
        $this->_error = false;
        if( $this->_query  = $this->_pdo->prepare( $sql )) {
            //echo "success";
            $x=1;
            if( count( $params ) ) {
                    foreach( $params as $param ) {
                        $this->_query->bindValue( $x, $param );
                        $x++;
                    }
                if( $this->_query->execute( ) ) {
                  //echo "success";
                    $this->_results = $this->_query->fetchAll( PDO::FETCH_OBJ );
                    $this->_count = $this->_query->rowCount();
                } else {
                    $this->_error = true;
                }   //end of if else
            }//end of outer if
            
        }//end of main if
        return $this;  
    }//end of function
    
    public function action( $action, $table, $where = array() ) {
        if( count( $where === 3 ) ) {
            $operators = array( '=', '>', '<', '>=', '<=' );
            
            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];
            
            if(in_array(  $operator, $operators ) ) {
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
                
                if( !$this->query( $sql, array( $value ))->error() ) {
                    return $this;
                }
            }
        }//end of if
        return false;
    }//end of function
    
    public function get( $table, $where ) {
        return $this->action('SELECT *', $table, $where);
    }//end of function
    
    public function insert( $table, $fields = array() ){
        if( count( $fields ) ){
            $keys  = array_keys( $fields );
            $values = '';
                       
            foreach( $fields as $field  ) {
                $values.='?, ';
            }
            $values  = rtrim( trim( $values ), ',' );
            $sql = " INSERT INTO "
                    . $table ." (`" . implode( '`, `', $keys) . "`) "
                    . "VALUES ( $values )";

                if( !$this->query( $sql, $fields )->error() ) {
                    return true;
                }
        
        }
        
        return false;
        
    }
    public function update( $table, $id, $fields = array() ) {
        $set = '';
        
        foreach ( $fields as $name => $value ) {
            $set .= $name." = ?, ";
        }
        $set  = rtrim( trim( $set ), ',' );
       // die($values);
        $sql = "UPDATE $table SET $set WHERE id = $id ";
        
        if( !$this->query( $sql, $fields )->error() ) {
            return true;
        }
        return false;
    }
    
    public  function  paging ( $sql, $records_per_page = 0,$page_no = '' ) {
        $starting_position=0;
        if( !empty( $page_no )) {
                $starting_position=($page_no - 1)*$records_per_page;
        }
        return  $sql." limit $starting_position,$records_per_page";
    }
       
    
    public function fetchData( $sql ) {
        $this->_error = false;
        if( $this->_query  = $this->_pdo->prepare( $sql ) ) {
            if( $this->_query->execute( ) ) {
                  //echo "success";
                    $this->_results = $this->_query->fetchAll( PDO::FETCH_ASSOC );
                    $this->_count = $this->_query->rowCount();
                } else {
                    $this->_error = true;
                }   //end of if else
        }
         return $this;
    }
    
    

    public function delete( $table, $where ) {
        return $this->action('DELETE', $table, $where);
    }//end of function
    
    public function results(){
     return $this->_results;    
    }//end of function
    
    public function error() {
        return $this->_error;
    }//end of function
    
    public function count() {
        return $this->_count;
    }//end of function
    
    public function first(){
        return $this->results()[0];
    }
    public function runQuery( $sql ){
        $this->_error = false;
        $data = Null;
        if( $data  = $this->_pdo->prepare( $sql ) ) {
            if( $data->execute( ) ) {
                    $data = $data->fetchAll( PDO::FETCH_ASSOC );
                    return $data;
                } else {
                    $this->_error = true;
                }   //end of if else
        }
         return false;
    }
    
    public function getLastInsertId(){
        return json_encode( array("id" => $this->_pdo->lastInsertId()) ); 
    } 
    
    
}

